# It is the code from  [Dynamic-DeepHit](https://github.com/chl8856/Dynamic-DeepHit) and [Bayesian LSTM](https://github.com/JP-MRPhys/bayesianLSTM/)
Title: "Dynamic-DeepHit: A Deep Learning Approach for Dynamic Survival Analysis With Competing Risks Based on Longitudinal Data"

Authors: Changhee Lee, Jinsung Yoon, Mihaela van der Schaar

- Reference: C. Lee, J. Yoon, M. van der Schaar, "Dynamic-DeepHit: A Deep Learning Approach for Dynamic Survival Analysis With Competing Risks Based on Longitudinal Data," IEEE Transactions on Biomedical Engineering (TBME). 2020
- Paper: https://ieeexplore.ieee.org/document/8681104

### Description of the code
* Minor modification from Dynamic-Deephit and Bayesian LSTM, upgrade to tensorflow version 2.
* No competing risk in PBC data, only one event in this study.



